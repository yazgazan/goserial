
all:	up run

build:
	GOOS=linux GOARCH=arm GOARM=7 go build

up: build
	ssh alarm sudo systemctl stop goserial
	scp goserial alarm:/home/yazou/gocode/src/github.com/yazgazan/goserial/
	ssh alarm sudo systemctl start goserial

run:
	ssh alarm sudo systemctl restart goserial

