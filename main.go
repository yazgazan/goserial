
package main

import (
  "io"
  "log"
  "time"
  "strings"
  "net/http"

  "github.com/tarm/goserial"
)

func main() {
  s := handleSerial()
  c := make(chan int)

  http.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
    connHandler(w, req, s, c)
  })
  give(c)

  err := http.ListenAndServe("localhost:5132", nil)
  if err != nil {
    log.Fatal(err)
  }
}

func give(c chan int) {
  go (func () {
    c <- 1
  })()
}

func take(c chan int) {
  <-c
}

func connHandler(w http.ResponseWriter, req *http.Request,
s io.ReadWriteCloser, c chan int) {
  path := req.URL.Path

  if path[:5] != "/cmd/" {
    log.Printf("malformated url: '%s'\n", path)
    return
  }
  cmd := strings.Replace(path[5:], "/", "", -1)
  take(c)
  s.Write([]byte(cmd))

  time.Sleep(500 * time.Millisecond)
  rep := ""
  for strings.Index(rep, "end\r\n") == -1 {
    b := make([]byte, 128)
    n, err := s.Read(b)
    if err != nil {
      log.Printf("Error (1): %s\n", err)
      give(c)
      w.Write([]byte(rep))
      return
    }
    if n == 0 {
      give(c)
      w.Write([]byte(rep))
      return
    }
    rep += string(b[:n])
  }

  w.Write([]byte(rep))
  give(c)
}

func handleSerial() io.ReadWriteCloser {
  c := &serial.Config{Name: "/dev/ttyACM0", Baud: 115200}
  s, err := serial.OpenPort(c)
  buf := ""

  time.Sleep(500 * time.Millisecond)
  if err != nil {
    log.Fatal(err)
  }
  for strings.Index(buf, "ready") == -1{
    b := make([]byte, 128)

    n, err := s.Read(b)
    if err != nil {
      log.Printf("Error (2): %s\n", err)
      break
    }
    if n == 0 {
      break
    }
    buf += string(b[:n])
  }
  return s
  /* for { */
  /*   buf := make([]byte, 128) */
  /*   n, err := s.Read(buf) */
  /*   if (err != nil) && (err != io.EOF) { */
  /*     log.Fatal(err) */
  /*     break */
  /*   } else if err == nil { */
  /*     fmt.Printf("%s", string(buf[:n])) */
  /*   } */
  /* } */
}

